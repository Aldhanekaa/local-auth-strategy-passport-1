'use strict';
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser')
const myDB = require('./connection');
const fccTesting = require('./freeCodeCamp/fcctesting.js');
const session = require('express-session');
const passport = require('passport');

const passportConfig = require('./functions/passportConfig');
const MainRouter = require('./routes/main')

let findUserDocument = require('./strategies/local')

const PassportConf = require('./functions/passport')

const app = express();
app.set("view engine", "pug");
app.set("views", `${__dirname}/views/pug`);

fccTesting(app); //For FCC testing purposes
app.use('/public', express.static(process.cwd() + '/public'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(bodyParser.urlencoded({ extended: false }))

passportConfig(app)

// app.get('/', (req, res) => {
//   res.send("loading..")
// })

// console.log(process.env.mongodbURI);

myDB(async (client) => {
  const myDataBase = await client.db('localAdvancedNode').collection('users');
  console.log("BREH")
  // Be sure to change the title

  MainRouter(app)

  PassportConf(passport, findUserDocument, myDataBase);


  // console.log("pa ssport", passport)

}).catch((e) => {
  console.log(e)
  app.route('/').get((req, res) => {
    res.render('index', { title: e, message: 'Unable to login' });
  });
});

function ensureAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/');
};

app.listen(process.env.PORT || 3000, () => {
  console.log('Listening on port ' + process.env.PORT);
})