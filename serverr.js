'use strict';
require('dotenv').config();
const express = require('express');
const myDB = require('./connection');
const fccTesting = require('./freeCodeCamp/fcctesting.js');
const session = require('express-session');
var bodyParser = require('body-parser')
const passport = require('passport');
const ObjectID = require("mongodb").ObjectID;
const LocalStrategy = require("passport-local");
const { rawListeners } = require('process');

const app = express();
app.set("view engine", "pug");
app.set("views", `${__dirname}/views/pug`);

fccTesting(app); //For FCC testing purposes
app.use('/public', express.static(process.cwd() + '/public'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));

app.use(passport.initialize());
app.use(passport.session());

// app.get('/', (req, res) => {
//   res.send("loading..")
// })

// console.log(process.env.mongodbURI);

myDB(async (client) => {
    const myDataBase = await client.db('localAdvancedNode').collection('users');
    console.log("BREH")
    // Be sure to change the title
    app.route('/').get((req, res) => {
        // Change the response to render the Pug template
        res.render('index', {
            title: 'Connected to Database',
            message: 'Please login',
            showLogin: true
        });
    });

    app.route("/login")
        .post(
            passport.authenticate('local', { failureRedirect: '/hello' }),
            (req, res) => {
                console.log("=== Post Login ===")
                // console.log("user", req.user == null)
                if (req.user != null) {
                    res.render("profile", { name: req.user.name })
                } else {
                    res.send('/fail')
                }
                // console.log("user", req.user);
            }
        )

    let findUserDocument = new LocalStrategy(
        (username, password, done) => {
            console.log("==== findUserDocument ====")

            console.log("username", username);
            console.log("password", password)

            myDataBase.findOne(
                { username: username },
                (err, user) => {
                    console.log("user", user);
                    if (err) {
                        console.log("error", err)
                        return done(err);
                    }

                    if (!user) {
                        return done(null, false);
                    } else if (user.password !== password) {
                        return done(null, false)
                    }

                    console.log("NIC#!")

                    return done(null, user)
                }
            )
        }
    );


    // Serialization and deserialization here...
    passport.serializeUser((user, done) => {
        console.log("==== serializeUser ====")
        console.log("user", user)
        done(null, user._id);
    });


    passport.deserializeUser((userId, done) => {
        console.log("==== deserializeUser ====")
        console.log("userId ", userId)

        myDataBase.findOne(
            { _id: new ObjectID(userId) },
            (err, doc) => {
                console.log(doc)
                done(null, doc);
            }
        )
    })

    passport.use(findUserDocument);

    // console.log("pa ssport", passport)

}).catch((e) => {
    console.log(e)
    app.route('/').get((req, res) => {
        res.render('index', { title: e, message: 'Unable to login' });
    });
});

app.listen(process.env.PORT || 3000, () => {
    console.log('Listening on port ' + process.env.PORT);
})