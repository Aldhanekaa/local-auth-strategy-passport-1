const ObjectID = require("mongodb").ObjectID;
module.exports = (passport, findUserDocument, myDataBase) => {
    // Serialization and deserialization here...
    passport.serializeUser((user, done) => {
        console.log("==== serializeUser ====")
        console.log("user", user)
        done(null, user._id);
    });


    passport.deserializeUser((userId, done) => {
        console.log("==== deserializeUser ====")
        console.log("userId ", userId)

        myDataBase.findOne(
            { _id: new ObjectID(userId) },
            (err, doc) => {
                console.log(doc)
                done(null, doc);
            }
        )
    });

    findUserDocument = findUserDocument(myDataBase)

    passport.use(findUserDocument);

}