const ProfileRouter = require('./profile');
const LoginRouter = require('./login');
const LogoutRouter = require('./logout');
const HomeRouter = require('./index');
const RegisterRouter = require('./register');

module.exports = (app) => {
    app.use('/', HomeRouter);

    app.use('/login', LoginRouter)

    app.use('/profile', ProfileRouter);

    app.use('/logout', LogoutRouter)

    app.use('/register', RegisterRouter)


    app.use((req, res, next) => {
        res.status(404).type('text').send('Not Found');
    });
}