const router = require('express').Router()

router.route('/').get((req, res) => {
    // Change the response to render the Pug template
    res.render('index', {
        title: 'Connected to Database',
        message: 'Please login',
        showLogin: true,
        showRegistration: true
    });
});

module.exports = router