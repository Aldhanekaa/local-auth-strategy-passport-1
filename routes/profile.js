const router = require('express').Router();
const checkSession = require('../functions/checkSession')

router.route('/').get((req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/');
}, (req, res) => {
    console.log(req.user);
    res.render('profile', { username: req.user.username })
});

module.exports = router;