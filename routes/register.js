const router = require('express').Router();
const bcrypt = require('bcrypt');
const passport = require('passport');
const myDB = require('../connection');

myDB(async (client) => {
    const myDataBase = await client.db('localAdvancedNode').collection('users');


    router.route('/')
        .post((req, res, next) => {
            const hash = bcrypt.hashSync(req.body.password, 12)

            myDataBase.findOne(
                { username: req.body.username },
                (err, user) => {

                    if (err) {
                        next(err);
                    } else if (user) {
                        res.redirect('/');
                    } else {
                        myDataBase.insertOne({ username: req.body.username, password: hash }, (err, doc) => {
                            if (err) {
                                res.redirect('/');
                            } else {
                                next(null, doc);
                            }
                        });
                    }
                })
        }, passport.authenticate('local', { failureRedirect: "/" }),
            (req, res) => {
                res.redirect('/profile')
            })

})
module.exports = router