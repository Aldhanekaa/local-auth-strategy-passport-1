const router = require('express').Router();
const passport = require('passport');

router.route("/")
    .get((req, res) => { res.redirect('/') })
    .post(
        passport.authenticate('local', { failureRedirect: '/' }),
        (req, res) => {
            console.log("=== Post Login ===")
            // console.log("user", req.user == null)
            if (req.user != null) {
                res.redirect("/profile")
            } else {
                res.send('Failed to login')
            }
            // console.log("user", req.user);
        }
    );

module.exports = router